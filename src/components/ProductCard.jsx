

import React from "react";
import { Card, StyledBody } from "baseui/card";
import { Button } from "baseui/button";
import AddToCart from "./AddToCart";
import ShoppingCartDisplay from "./ShoppingCartDisplay";

const ProductCard = ({ productName, imageUrl, quantitySold, color, ram, price }) => {
  const [isAddedToCart, setAddedToCart] = React.useState(false);
  const [cartItems, setCartItems] = React.useState([]);

  const handleAddToCart = () => {
    setAddedToCart(true);
    setCartItems((prevItems) => [...prevItems, { productName, imageUrl, quantitySold, color, ram, price }]);
  };

  return (
    <Card
      overrides={{
        Root: { style: { width: "300px", margin: "16px", textAlign: "center" } },
        Contents: { style: { textAlign: "center" } },
      }}
    >
      <img src={imageUrl} alt={productName} style={{ width: "100%", height: "200px", objectFit: "cover" }} />
      <StyledBody>
        <h3>{productName}</h3>
        <p>Color: {color}</p>
        <p>RAM: {ram}</p>
        {/* <p>Price: ${price}</p> */}
        <h5>
          <b>{quantitySold}</b>
          {quantitySold < 2 ? " Item" : " Items"} sold
        </h5>
      </StyledBody>
      <div style={{ display: "flex", flexDirection: "column", gap: "8px", marginBottom: "16px" }}>
        {!isAddedToCart ? (
          <Button
            onClick={handleAddToCart}
            overrides={{
              BaseButton: { style: { width: "100%" } },
            }}
          >
            Add to Cart
          </Button>
        ) : (
          <>
            <AddToCart productName={productName} imageUrl={imageUrl} quantitySold={quantitySold} color={color} ram={ram} />
            <ShoppingCartDisplay cartItems={cartItems} />
          </>
        )}
        <Button
          overrides={{
            BaseButton: { style: { width: "100%" } },
          }}
        >
          Buy Now!
        </Button>
      </div>
    </Card>
  );
};

export default ProductCard;
