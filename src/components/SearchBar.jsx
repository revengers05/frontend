import React from "react";
import { Input } from "baseui/input";

const SearchBar = (props) => {
  const [value, setValue] = React.useState("");
  const {onChange}=props;
  return (
    <div
      className="search-bar"
      style={{
        marginLeft: "50px",
        marginRight: "50px",
        marginBottom: "20px"
      }}
    >
      <Input
        value={value}
        placeholder="Search products..."
        onChange={e=>setValue(e.target.value)}
        onKeyDown={(e)=>{
          console.log("Inside Key Press",e.key)
          if(e.key === "Enter"){
            onChange(value)
          }
        }}
        overrides={{ Input: { style: { width: "2000px" } } }
      }
      />
    </div>
  );
};

export default SearchBar;
