import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { StyledBody } from "baseui/card";
import TitleBarUserLoggedIn from "./TitleBarUserLoggedIn";

const MerchantLogin = () => {
  const [products, setProducts] = useState([
    {
      productName: "JIONee",
      imageUrl:
        "https://img.freepik.com/free-photo/blank-phone-screen-purple-background_53876-143196.jpg",
      totalstock: 4,
      price: 9999,
      id: 1, // Add a unique identifier for each product
    },
    // Add more sample products if needed
  ]);
  const [quantities, setQuantities] = useState({});
  const userId = localStorage.getItem("userId");

  console.log("Yayyy");
  console.log(userId);
  useEffect(() => {
    // Fetch products when the component is mounted
    fetchMerchantProducts();
  }, []);

  const fetchMerchantProducts = async () => {
    try {
      const response = await fetch(
        `http://10.65.1.103:8082/merchant-service/v1.0/merchant-service/merchant/products?merchantId=${userId}`
      );

      if (response.ok) {
        const data = await response.json();
        console.log(data);
        // Assuming the response is an array of products
        setProducts(data);
        // Initialize quantities state with default values
        const initialQuantities = {};
        data.forEach((product) => {
          initialQuantities[product.id] = 0;
        });
        setQuantities(initialQuantities);
      } else {
        console.error("Error fetching merchant products");
      }
    } catch (error) {
      console.error("Error during fetch:", error);
    }
  };

  const handleQuantityChange = async (productId, change) => {
    try {
      const response = await fetch(
        `http://10.65.1.103:8082/merchant-service/v1.0/merchant-service/updateProduct/count?productId=${productId}&merchantId=${userId}&count=${change}`,
        {
          method: "PUT", // or "POST" based on your API
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(),
        }
      );

      if (response.ok) {
        // Update local quantities state if the server update is successful
        setQuantities((prevQuantities) => ({
          ...prevQuantities,
          [productId]: Math.max(0, prevQuantities[productId] + change),
        }));
      } else {
        console.error("Error updating quantity on the server");
      }
    } catch (error) {
      console.error("Error during fetch:", error);
    }
  };

  return (
    <div>
      <TitleBarUserLoggedIn />

      <div>
        <h2>Welcome</h2>

        {/* Add Products Button */}
        <Link to="/add-products">
          <button
            style={{
              position: "absolute",
              top: "10px",
              right: "10px",
              backgroundColor: "#007BFF",
              color: "#fff",
              padding: "10px",
              borderRadius: "4px",
              cursor: "pointer",
            }}
          >
            Add Products
          </button>
        </Link>

        {/* Display Products */}
        <div style={{ display: "flex", flexWrap: "wrap" }}>
          {products.map((product) => (
            <div
              key={product.id}
              style={{
                width: "calc(25% - 20px)", // 4 cards in a row with margin
                border: "2px solid #000", // Double thickness black border added
                padding: "10px",
                margin: "10px",
                borderRadius: "8px",
                position: "relative",
                boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)", // Adding a subtle shadow
                transition: "transform 0.3s ease-in-out",
                ":hover": { transform: "scale(1.05)" }, // Hover effect
                background: "#fff",
                overflow: "hidden",
                display: "flex",
                flexDirection: "column", // Display child elements in a column
              }}
            >
              <img
                src={product.imageUrl}
                alt={product.productName}
                style={{
                  width: "100%",
                  height: "200px",
                  objectFit: "cover",
                  borderRadius: "8px 8px 0 0", // Rounded corners for the top
                }}
              />
              <StyledBody
                style={{
                  padding: "15px",
                  textAlign: "left",
                  flex: "1", // Take remaining space
                }}
              >
                <p
                  style={{
                    fontWeight: "bold",
                    fontSize: "18px",
                    margin: "0 0 10px",
                  }}
                >
                  {product.productName}
                </p>
                <p style={{ margin: "0 0 5px" }}>Price: ${product.price}</p>
                <p style={{ margin: "0 0 5px" }}>
                  Current Stock: {product.totalstock}
                </p>
              </StyledBody>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  padding: "10px",
                  background: "#000", // Black color for button bar
                  color: "#fff", // White color for button bar text
                }}
              >
                <button
                  type="button"
                  onClick={() => handleQuantityChange(product.id, -1)}
                  style={{
                    padding: "8px",
                    backgroundColor: "#fff", // White color for button
                    color: "#000", // Black color for button text
                    border: "2px solid #000", // Double thickness black border for button
                    borderRadius: "4px",
                    cursor: "pointer",
                  }}
                >
                  -
                </button>

                <button
                  type="button"
                  onClick={() => handleQuantityChange(product.id, 1)}
                  style={{
                    padding: "8px",
                    backgroundColor: "#fff", // White color for button
                    color: "#000", // Black color for button text
                    border: "2px solid #000", // Double thickness black border for button
                    borderRadius: "4px",
                    cursor: "pointer",
                  }}
                >
                  +
                </button>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default MerchantLogin;
