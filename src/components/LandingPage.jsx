import React, { useState, useEffect } from "react";
import { StyledBody } from "baseui/card";
import SearchBar from "./SearchBar";
import SearchResults from "./SearchResults";
import TitleBar from "./TitleBar";
import AuthSection from "./AuthSection";
import ProductCard from "./ProductCard";
import ProductDetails from "./ProductDetails";

const LandingPage = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [topProducts, setTopProducts] = useState([]);
  const [selectedProductId, setSelectedProductId] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    // Fetch top products when the component mounts
    fetch("http://10.65.1.103:8081/product-service/v1.0/product/topProducts")
      .then((response) => response.json())
      .then((data) => {
        setTopProducts(data);
      })
      .catch((error) => {
        console.error("Error fetching top products:", error);
      });
  }, []);

  useEffect(() => {
    if (searchTerm) {
      const apiUrl = searchTerm
        ? `http://10.65.1.103:8084/v1.0/search/products?query=${encodeURIComponent(
            searchTerm
          )}`
        : "http://10.65.1.103:8081/product-service/v1.0/product/getAllProducts";

      fetch(apiUrl)
        .then((response) => response.json())
        .then((data) => {
          setSearchResults(data);
          setLoading(false);

          // Check if no results found and navigate
          if (data.length === 0 && searchTerm.trim() !== "") {
            // Display alert instead of rendering a separate element
            window.alert("No results found. Redirecting to Home");
            // You need to handle navigation, e.g., using React Router
            // navigate("/");
          }
        })
        .catch((error) => {
          setError(error);
          setLoading(false);
        });
    }
  }, [searchTerm]);

  const handleSearchChange = (value) => {
    setSearchTerm(value);
    setSelectedProductId(null); // Reset selectedProductId when the search term changes
  };

  // if (loading) {
  //   return <div>Loading...</div>;
  // }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <div>
      <header>
        <div className="d-flex">
          <TitleBar />
        </div>
        <SearchBar onChange={handleSearchChange} />
      </header>

      <StyledBody>
        {selectedProductId ? (
          <ProductDetails productId={selectedProductId} />
        ) : (
          <>
            <h1>
              <center>Top Mobile Phones</center>
            </h1>
            {searchTerm.trim() === "" ? (
              <div
                style={{
                  display: "flex",
                  flexWrap: "wrap",
                  justifyContent: "center",
                }}
              >
                {topProducts.map((product) => (
                  <div
                    key={product.id}
                    onClick={() => setSelectedProductId(product.id)}
                  >
                    <ProductCard
                      productName={product.productName}
                      quantitySold={product.quantitySold}
                      ram={product.ram}
                      color={product.color}
                      imageUrl={product.imageUrl}
                    />
                  </div>
                ))}
              </div>
            ) : (
              <SearchResults
                searchResults={searchResults}
                onProductClick={(productId) => setSelectedProductId(productId)}
              />
            )}
          </>
        )}
      </StyledBody>
    </div>
  );
};

export default LandingPage;
