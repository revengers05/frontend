import React, { useState, useEffect } from "react";
import { StyledBody } from "baseui/card";
import SearchBar from "./SearchBar";
import ProductCard from "./ProductCard";
import TitleBarUserLoggedIn from "./TitleBarUserLoggedIn";
const UserLoggedIn = () => {
  const [topProducts, setTopProducts] = useState([]);

  useEffect(() => {
    fetch("http://10.65.1.103:8081/product-service/v1.0/product/topProducts")
      .then((response) => response.json()) // Parse the response as JSON
      .then((data) => {
        setTopProducts(data); // Set the data in state
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  return (
    <div>
      <header>
        <div className="d-flex">
          <TitleBarUserLoggedIn></TitleBarUserLoggedIn>
        </div>
        <SearchBar />
      </header>
      <StyledBody>
        <h1>
          <center>Top Mobile Phones</center>
        </h1>
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
          {topProducts.map((product) => (
            <ProductCard
              key={product.productId}
              productName={product.productName}
              quantity={product.quantity}
              ram={product.ram}
              color={product.color}
              imageUrl={product.imageUrl}
            />
          ))}
        </div>
      </StyledBody>
    </div>
  );
};
export default UserLoggedIn;
