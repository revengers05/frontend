import React from "react";
import { Card, StyledBody } from "baseui/card";
const AddToCart = ({ productName, imageUrl, quantity, color, ram }) => {
  return (
    <Card
      overrides={{
        Root: { style: { width: "300px", margin: "16px", textAlign: "center" } },
        Contents: { style: { textAlign: "center" } },
      }}
    >
      <img src={imageUrl} alt={productName} style={{ width: "100%", height: "200px", objectFit: "cover" }} />
      <StyledBody>
        <h3>{productName}</h3>
        <p>Color: {color}</p>
        <p>RAM: {ram}</p>
        <h5>
          <b>{quantity}</b>
          {quantity < 2 ? " Item" : " Items"} added to the cart
        </h5>
      </StyledBody>
    </Card>
  );
};

export default AddToCart;
