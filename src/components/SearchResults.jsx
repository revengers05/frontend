// SearchResults.jsx
import React from "react";
import ProductCard from "./ProductCard";

const SearchResults = (props) => {
  const { searchResults, onProductClick } = props
  return (
    <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
      {searchResults?.map((product) => (
        <div key={product.productId} onClick={() => onProductClick(product.productId)}>
          <ProductCard
            productName={product.productName}
            quantitySold={product.quantity} // Assuming quantitySold is what you want to display
            ram={product.ram}
            color={product.color}
            imageUrl={product.imageUrl}
          />
        </div>
      ))}
    </div>
  );
};

export default SearchResults;
