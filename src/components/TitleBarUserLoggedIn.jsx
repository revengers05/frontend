// TitleBar.js
import React from "react";
import AuthSection from "./AuthSection";
import { Link } from "react-router-dom";

const TitleBarUserLoggedIn = () => {
  // Check if the user is logged in (you can modify this condition based on your authentication logic)
  const isLoggedIn = localStorage.getItem("token") !== null;

  return (
    <div>
      <Link to="/">
        <img
          src="https://as1.ftcdn.net/v2/jpg/01/72/53/98/1000_F_172539855_qUkbwZ7BZebkNsRjK5Vebt0FMl1jBDXs.jpg"
          alt="My-logo"
          width="50px"
          height="50px"
        ></img>
      </Link>
      <img
        style={{ marginLeft: "1320px" }}
        src="https://static.vecteezy.com/system/resources/previews/006/732/119/non_2x/account-icon-sign-symbol-logo-design-free-vector.jpg"
        alt="My-logo"
        width="50px"
        height="50px"
      ></img>
    </div>
  );
};

export default TitleBarUserLoggedIn;
