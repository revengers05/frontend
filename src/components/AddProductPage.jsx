import React, { useState } from "react";
import TitleBarUserLoggedIn from "./TitleBarUserLoggedIn";

const AddProductPage = () => {
  const [productData, setProductData] = useState({
    productName: "",
    merchantId: 123, // Replace with the actual merchant ID
    productId: "",
    imageUrl: "",
    usp: "",
    stock: 0,
    price: 0.0,
    description: "",
    ram: "",
    color: "",
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setProductData((prevData) => ({ ...prevData, [name]: value }));
  };

  const handleAddProduct = async () => {
    try {
      const response = await fetch("YOUR_ADD_PRODUCT_ENDPOINT", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(productData),
      });

      if (response.ok) {
        // Product added successfully, handle any additional logic here
        console.log("Product added successfully");
      } else {
        console.error("Error adding product");
      }
    } catch (error) {
      console.error("Error during fetch:", error);
    }
  };

  return (
    <div>
      <h2>Add Products</h2>
      <form>
        {/* Product Name */}
        <div>
          <label htmlFor="productName">Product Name:</label>
          <input
            type="text"
            id="productName"
            name="productName"
            value={productData.productName}
            onChange={handleInputChange}
          />
        </div>

        {/* Merchant ID (Hidden, can be set dynamically or from authentication) */}
        <input type="hidden" name="merchantId" value={productData.merchantId} />

        {/* Product ID (if needed, otherwise it can be generated on the server) */}
        <div>
          <label htmlFor="productId">Product ID:</label>
          <input
            type="text"
            id="productId"
            name="productId"
            value={productData.productId}
            onChange={handleInputChange}
          />
        </div>

        {/* Image URL */}
        <div>
          <label htmlFor="imageUrl">Image URL:</label>
          <input
            type="text"
            id="imageUrl"
            name="imageUrl"
            value={productData.imageUrl}
            onChange={handleInputChange}
          />
        </div>

        {/* Unique Selling Proposition (USP) */}
        <div>
          <label htmlFor="usp">USP:</label>
          <input
            type="text"
            id="usp"
            name="usp"
            value={productData.usp}
            onChange={handleInputChange}
          />
        </div>

        {/* Stock */}
        <div>
          <label htmlFor="stock">Stock:</label>
          <input
            type="number"
            id="stock"
            name="stock"
            value={productData.stock}
            onChange={handleInputChange}
          />
        </div>

        {/* Price */}
        <div>
          <label htmlFor="price">Price:</label>
          <input
            type="number"
            id="price"
            name="price"
            value={productData.price}
            onChange={handleInputChange}
          />
        </div>

        {/* Description */}
        <div>
          <label htmlFor="description">Description:</label>
          <textarea
            id="description"
            name="description"
            value={productData.description}
            onChange={handleInputChange}
          />
        </div>

        {/* RAM */}
        <div>
          <label htmlFor="ram">RAM:</label>
          <input
            type="text"
            id="ram"
            name="ram"
            value={productData.ram}
            onChange={handleInputChange}
          />
        </div>

        {/* Color */}
        <div>
          <label htmlFor="color">Color:</label>
          <input
            type="text"
            id="color"
            name="color"
            value={productData.color}
            onChange={handleInputChange}
          />
        </div>

        {/* Submit Button */}
        <button type="button" onClick={handleAddProduct}>
          Add Product
        </button>
      </form>
    </div>
  );
};

export default AddProductPage;
