// ShoppingCartDisplay.jsx

import React from "react";
import { Link } from "react-router-dom"; // Assuming you are using React Router
import { Card, StyledBody } from "baseui/card";
import TitleBarUserLoggedIn from "./TitleBarUserLoggedIn";

const ShoppingCartDisplay = ({ cartItems }) => {
  return (
    <div>
      <TitleBarUserLoggedIn></TitleBarUserLoggedIn>

      <Card
        overrides={{
          Root: {
            style: { width: "300px", margin: "16px", textAlign: "center" },
          },
          Contents: { style: { textAlign: "center" } },
        }}
      >
        <img
          src="/path/to/cart-image.png"
          alt="Cart"
          style={{ width: "100%", height: "200px", objectFit: "cover" }}
        />
        <StyledBody>
          <h3>Shopping Cart</h3>
          <ul>
            {Array.isArray(cartItems) ? (
              cartItems.map((item, index) => (
                <li key={index}>
                  {item.productName} - {item.quantity}{" "}
                  {item.quantity === 1 ? "item" : "items"}
                </li>
              ))
            ) : (
              <p>No items in the cart</p>
            )}
          </ul>
          <Link to="/cart">View Cart</Link>
        </StyledBody>
      </Card>
    </div>
  );
};

export default ShoppingCartDisplay;
