import React, { useState, useEffect } from "react";

const ProductDetails = ({ productId }) => {
  const [productDetails, setProductDetails] = useState(null);
  const [similarMerchants, setSimilarMerchants] = useState([]);

  useEffect(() => {
    // Fetch product details by productId
    fetch(`http://10.65.1.103:8081/product-service/v1.0/product/getProductById?productId=${productId}`)
      .then((response) => response.json())
      .then((data) => {
        setProductDetails(data);
      })
      .catch((error) => {
        console.error("Error fetching product details:", error);
      });

    // Placeholder for fetching similar merchants
    fetch(`http://10.1.65.103:8082/merchant-service/v1.0/merchant-service/product/merchant/${productId}`)
      .then((response) => response.json())
      .then((data) => {
        setSimilarMerchants(data);
      })
      .catch((error) => {
        console.error("Error fetching similar merchants:", error);
      });
  }, [productId]);

  if (!productDetails) {
    return <div>Loading...</div>; // You can customize the loading state
  }

  return (
    <div>
      <h2>{productDetails.productName} Details</h2>
      <img src={productDetails.imageUrl} alt={productDetails.productName} style={{ width: "100%", height: "400px", objectFit: "cover" }} />
      <p>Price: {productDetails.price}</p>
      <p>RAM: {productDetails.ram}</p>
      {/* Add more details based on your product schema */}

      <h3>People Also Buy From</h3>
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {similarMerchants.map((merchant) => (
          <div
            key={merchant.merchantId}
            style={{
              width: "calc(33% - 20px)",
              border: "1px solid #ccc",
              padding: "10px",
              margin: "10px",
              borderRadius: "8px",
              position: "relative",
            }}
          >
            <img src={merchant.imageUrl} alt={merchant.merchantName} style={{ width: "100%", height: "200px", objectFit: "cover" }} />
            <p>{merchant.merchantName}</p>
            {/* Add more merchant details based on your merchant schema */}
          </div>
        ))}
      </div>
    </div>
  );
};

export default ProductDetails;
