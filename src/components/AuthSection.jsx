import React from "react";

const AuthSection = () => {
  return (
    <div className="auth-section" style={{ marginLeft: "1320px" }}>
      <a href="/signUp">Signup</a> | <a href="/login">Login</a>
    </div>
  );
};

export default AuthSection;
