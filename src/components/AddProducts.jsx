import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import TitleBarUserLoggedIn from "./TitleBarUserLoggedIn";

const AddProducts = ({ onAddProduct }) => {
  const navigate = useNavigate();
  const userId = localStorage.getItem("userId");
  const [productData, setProductData] = useState({
    productName: "",
    merchantId: userId,
    imageUrl: "",
    usp: "",
    stock: "",
    price: "",
    description: "",
    ram: "",
    color: "",
  });

  const handleInputChange = (e) => {
    console.log("Event in handleInputChange", e);
    setProductData({
      ...productData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Assuming you have an endpoint to generate productId on the server
    try {
      const response = await fetch(
        "http://10.65.1.103:8082/merchant-service/v1.0/merchant-service/addProduct",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(productData),
        }
      );

      if (response.ok) {
        alert("Product Added");

        // Call the function to handle the form submission
        // onAddProduct(productData);

        // Reset the form after submission
        setProductData({
          productName: "",
          merchantId: userId,
          imageUrl: "",
          usp: "",
          stock: "",
          price: "",
          description: "",
          ram: "",
          color: "",
        });

        // Use navigate to redirect to the merchant login page
        navigate("/merchantpage");
      } else {
        console.error("Error generating productId");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  return (
    <div>
      <TitleBarUserLoggedIn></TitleBarUserLoggedIn>

      <div
        style={{
          width: "300px",
          margin: "auto",
          fontFamily: "Arial, sans-serif",
        }}
      >
        <h2 style={{ textAlign: "center", color: "#007BFF" }}>Add Product</h2>
        <form onSubmit={handleSubmit}>
          {/* Product Name */}
          <div style={{ marginBottom: "15px" }}>
            <label
              style={{ display: "block", marginBottom: "5px", color: "#555" }}
            >
              Product Name:
            </label>
            <input
              type="text"
              name="productName"
              value={productData.productName}
              onChange={handleInputChange}
              style={inputStyle}
              required
            />
          </div>

          {/* Image URL */}
          <div style={{ marginBottom: "15px" }}>
            <label
              style={{ display: "block", marginBottom: "5px", color: "#555" }}
            >
              Image URL:
            </label>
            <input
              type="text"
              name="imageUrl"
              value={productData.imageUrl}
              onChange={handleInputChange}
              style={inputStyle}
              required
            />
          </div>

          {/* USP */}
          <div style={{ marginBottom: "15px" }}>
            <label
              style={{ display: "block", marginBottom: "5px", color: "#555" }}
            >
              USP:
            </label>
            <input
              type="text"
              name="usp"
              value={productData.usp}
              onChange={handleInputChange}
              style={inputStyle}
              required
            />
          </div>

          {/* Stock */}
          <div style={{ marginBottom: "15px" }}>
            <label
              style={{ display: "block", marginBottom: "5px", color: "#555" }}
            >
              Stock:
            </label>
            <input
              type="number"
              name="stock"
              min="0"
              max="500"
              value={productData.stock}
              onChange={handleInputChange}
              style={inputStyle}
              required
            />
          </div>

          {/* Price */}
          <div style={{ marginBottom: "15px" }}>
            <label
              style={{ display: "block", marginBottom: "5px", color: "#555" }}
            >
              Price:
            </label>
            <input
              type="number"
              name="price"
              min="1000"
              max="100000"
              value={productData.price}
              onChange={handleInputChange}
              style={inputStyle}
              required
            />
          </div>

          {/* Description */}
          <div style={{ marginBottom: "15px" }}>
            <label
              style={{ display: "block", marginBottom: "5px", color: "#555" }}
            >
              Description:
            </label>
            <input
              type="text"
              name="description"
              value={productData.description}
              onChange={handleInputChange}
              style={inputStyle}
              required
            />
          </div>

          {/* RAM */}
          <div style={{ marginBottom: "15px" }}>
            <label
              style={{ display: "block", marginBottom: "5px", color: "#555" }}
            >
              RAM:
            </label>
            <input
              type="number"
              name="ram"
              min="2"
              max="20"
              value={productData.ram}
              onChange={handleInputChange}
              style={inputStyle}
              required
            />
          </div>

          {/* Color */}
          <div style={{ marginBottom: "15px" }}>
            <label
              style={{ display: "block", marginBottom: "5px", color: "#555" }}
            >
              Color:
            </label>
            <input
              type="text"
              name="color"
              value={productData.color}
              onChange={handleInputChange}
              style={inputStyle}
              required
            />
          </div>

          {/* Submit Button */}
          <button type="submit" style={buttonStyle}>
            Add Product
          </button>
        </form>
      </div>
    </div>
  );
};

const inputStyle = {
  width: "100%",
  padding: "8px",
  boxSizing: "border-box",
  borderRadius: "4px",
  border: "1px solid #ccc",
};

const buttonStyle = {
  backgroundColor: "#007BFF",
  color: "#fff",
  padding: "10px",
  borderRadius: "4px",
  cursor: "pointer",
  width: "100%",
  border: "none",
};

export default AddProducts;
