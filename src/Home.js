import React from "react";
import LandingPage from "./components/LandingPage";
import { Provider as StyletronProvider } from "styletron-react";
import { Client as Styletron } from "styletron-engine-atomic";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

const engine = new Styletron();

function Home() {
  return (
    <StyletronProvider value={engine}>
      <div className="App">
        <LandingPage />
      </div>
    </StyletronProvider>
  );
}

export default Home;
