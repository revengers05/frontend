import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import TitleBarLoginPage from "./components/TitleBarLoginPage";

const Login = () => {
  const navigate = useNavigate();
  const [merchantLogin, setMerchantLogin] = useState({
    email: "",
    password: "",
    type: "merchant",
  });
  const [userLogin, setUserLogin] = useState({
    email: "",
    password: "",
    type: "user",
  });
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const isValidEmail = (email) => {
    // A more comprehensive email validation regex
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return emailRegex.test(email);
  };

  const handleMerchantLoginChange = (e) => {
    setMerchantLogin({ ...merchantLogin, [e.target.name]: e.target.value });
  };

  const handleUserLoginChange = (e) => {
    setUserLogin({ ...userLogin, [e.target.name]: e.target.value });
  };

  const handleLogin = async (type) => {
    try {
      const loginData = type === "merchant" ? merchantLogin : userLogin;

      if (!isValidEmail(loginData.email)) {
        alert("Invalid email address");
        return;
      }

      const response = await fetch("http://localhost:8081/v1.0/users/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(loginData),
      });

      if (response.ok) {
        const result = await response.json();
        const { token, userId } = result;
        localStorage.setItem("token", token);
        localStorage.setItem("userId", userId);
        setIsLoggedIn(true);

        if (type === "merchant") {
          navigate("/merchantpage");
        } else {
          navigate("/loginpage");
        }
      } else {
        navigate("/");
        alert("Error during login");
      }
    } catch (error) {
      console.error("Error during login:", error);
    }
  };

  return (
    <div>
      <TitleBarLoginPage></TitleBarLoginPage>

      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
          height: "100vh",
        }}
      >
        {isLoggedIn ? (
          <p>You are already logged in.</p>
        ) : (
          <>
            {/* Merchant Login */}
            <div
              style={{
                width: "45%",
                padding: "20px",
                border: "1px solid #ccc",
                borderRadius: "8px",
              }}
            >
              <h2 style={{ textAlign: "center", marginBottom: "20px" }}>
                Merchant Login
              </h2>
              <form>
                <div style={{ marginBottom: "15px" }}>
                  <label style={{ display: "block", marginBottom: "5px" }}>
                    Email:
                  </label>
                  <input
                    type="email"
                    name="email"
                    value={merchantLogin.email}
                    onChange={handleMerchantLoginChange}
                    pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"
                    required
                    style={{
                      width: "100%",
                      padding: "8px",
                      boxSizing: "border-box",
                      borderRadius: "4px",
                      border: "1px solid #ccc",
                    }}
                  />
                </div>
                <div>
                  <label style={{ display: "block", marginBottom: "5px" }}>
                    Password:
                  </label>
                  <input
                    type="password"
                    name="password"
                    value={merchantLogin.password}
                    onChange={handleMerchantLoginChange}
                    style={{
                      width: "100%",
                      padding: "8px",
                      boxSizing: "border-box",
                      borderRadius: "4px",
                      border: "1px solid #ccc",
                    }}
                    required
                  />
                </div>
                <button
                  type="button"
                  onClick={() => handleLogin("merchant")}
                  style={{
                    marginTop: "15px",
                    backgroundColor: "#007BFF",
                    color: "#fff",
                    padding: "10px",
                    borderRadius: "4px",
                    cursor: "pointer",
                  }}
                >
                  Login
                </button>
              </form>
            </div>

            {/* User Login */}
            <div
              style={{
                width: "45%",
                padding: "20px",
                border: "1px solid #ccc",
                borderRadius: "8px",
              }}
            >
              <h2 style={{ textAlign: "center", marginBottom: "20px" }}>
                User Login
              </h2>
              <form>
                <div style={{ marginBottom: "15px" }}>
                  <label style={{ display: "block", marginBottom: "5px" }}>
                    Email:
                  </label>
                  <input
                    type="email"
                    name="email"
                    value={userLogin.email}
                    onChange={handleUserLoginChange}
                    pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"
                    required
                    style={{
                      width: "100%",
                      padding: "8px",
                      boxSizing: "border-box",
                      borderRadius: "4px",
                      border: "1px solid #ccc",
                    }}
                  />
                </div>
                <div>
                  <label style={{ display: "block", marginBottom: "5px" }}>
                    Password:
                  </label>
                  <input
                    type="password"
                    name="password"
                    value={userLogin.password}
                    onChange={handleUserLoginChange}
                    style={{
                      width: "100%",
                      padding: "8px",
                      boxSizing: "border-box",
                      borderRadius: "4px",
                      border: "1px solid #ccc",
                    }}
                    required
                  />
                </div>
                <button
                  type="button"
                  onClick={() => handleLogin("user")}
                  style={{
                    marginTop: "15px",
                    backgroundColor: "#007BFF",
                    color: "#fff",
                    padding: "10px",
                    borderRadius: "4px",
                    cursor: "pointer",
                  }}
                >
                  Login
                </button>
              </form>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default Login;
