import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import TitleBarSignUpPage from "./components/TitleBarSignUpPage";

const SignUp = () => {
  const navigate = useNavigate();

  const [merchantSignUp, setMerchantSignUp] = useState({
    email: "",
    password: "",
    name: "",
    type: "merchant",
  });

  const [userSignUp, setUserSignUp] = useState({
    email: "",
    password: "",
    name: "",
    type: "user",
  });

  const isValidEmail = (email) => {
    // A more comprehensive email validation regex
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return emailRegex.test(email);
  };

  const handleMerchantSignUpChange = (e) => {
    setMerchantSignUp({ ...merchantSignUp, [e.target.name]: e.target.value });
  };

  const handleUserSignUpChange = (e) => {
    setUserSignUp({ ...userSignUp, [e.target.name]: e.target.value });
  };

  const handleSignUp = async (type) => {
    try {
      const signUpData = type === "merchant" ? merchantSignUp : userSignUp;

      if (!isValidEmail(signUpData.email)) {
        alert("Invalid email address");
        return;
      }

      const response = await fetch("http://localhost:8081/v1.0/users/signup", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(signUpData),
      });

      if (response.ok) {
        navigate("/login");
      } else {
        alert("Error during SignUp");
        navigate("/signUp");
      }
    } catch (error) {
      console.error("Error during SignUp:", error);
    }
  };

  return (
    <div>
      <TitleBarSignUpPage />
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
          height: "100vh",
        }}
      >
        {/* Merchant SignUp */}
        <div
          style={{
            width: "45%",
            padding: "20px",
            border: "1px solid #ccc",
            borderRadius: "8px",
          }}
        >
          <h2 style={{ textAlign: "center", marginBottom: "20px" }}>
            Merchant SignUp
          </h2>
          <form>
            <div style={{ marginBottom: "15px" }}>
              <label style={{ display: "block", marginBottom: "5px" }}>
                Email:
              </label>
              <input
                type="email"
                name="email"
                value={merchantSignUp.email}
                onChange={handleMerchantSignUpChange}
                required
                style={{
                  width: "100%",
                  padding: "8px",
                  boxSizing: "border-box",
                  borderRadius: "4px",
                  border: "1px solid #ccc",
                }}
              />
            </div>
            <div style={{ marginBottom: "15px" }}>
              <label style={{ display: "block", marginBottom: "5px" }}>
                Name:
              </label>
              <input
                type="text"
                name="name"
                value={merchantSignUp.name}
                onChange={handleMerchantSignUpChange}
                style={{
                  width: "100%",
                  padding: "8px",
                  boxSizing: "border-box",
                  borderRadius: "4px",
                  border: "1px solid #ccc",
                }}
              />
            </div>
            <div>
              <label style={{ display: "block", marginBottom: "5px" }}>
                Password:
              </label>
              <input
                type="password"
                name="password"
                value={merchantSignUp.password}
                onChange={handleMerchantSignUpChange}
                style={{
                  width: "100%",
                  padding: "8px",
                  boxSizing: "border-box",
                  borderRadius: "4px",
                  border: "1px solid #ccc",
                }}
              />
            </div>
            <button
              type="button"
              onClick={() => handleSignUp("merchant")}
              style={{
                marginTop: "15px",
                backgroundColor: "#007BFF",
                color: "#fff",
                padding: "10px",
                borderRadius: "4px",
                cursor: "pointer",
              }}
            >
              SignUp
            </button>
          </form>
        </div>

        {/* User SignUp */}
        <div
          style={{
            width: "45%",
            padding: "20px",
            border: "1px solid #ccc",
            borderRadius: "8px",
          }}
        >
          <h2 style={{ textAlign: "center", marginBottom: "20px" }}>
            User SignUp
          </h2>
          <form>
            <div style={{ marginBottom: "15px" }}>
              <label style={{ display: "block", marginBottom: "5px" }}>
                Email:
              </label>
              <input
                type="email"
                name="email"
                value={userSignUp.email}
                onChange={handleUserSignUpChange}
                required
                style={{
                  width: "100%",
                  padding: "8px",
                  boxSizing: "border-box",
                  borderRadius: "4px",
                  border: "1px solid #ccc",
                }}
              />
            </div>
            <div style={{ marginBottom: "15px" }}>
              <label style={{ display: "block", marginBottom: "5px" }}>
                Name:
              </label>
              <input
                type="text"
                name="name"
                value={userSignUp.name}
                onChange={handleUserSignUpChange}
                style={{
                  width: "100%",
                  padding: "8px",
                  boxSizing: "border-box",
                  borderRadius: "4px",
                  border: "1px solid #ccc",
                }}
              />
            </div>
            <div>
              <label style={{ display: "block", marginBottom: "5px" }}>
                Password:
              </label>
              <input
                type="password"
                name="password"
                value={userSignUp.password}
                onChange={handleUserSignUpChange}
                style={{
                  width: "100%",
                  padding: "8px",
                  boxSizing: "border-box",
                  borderRadius: "4px",
                  border: "1px solid #ccc",
                }}
              />
            </div>
            <button
              type="button"
              onClick={() => handleSignUp("user")}
              style={{
                marginTop: "15px",
                backgroundColor: "#007BFF",
                color: "#fff",
                padding: "10px",
                borderRadius: "4px",
                cursor: "pointer",
              }}
            >
              SignUp
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
