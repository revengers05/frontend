import React from "react";
import LandingPage from "./components/LandingPage";
import { Provider as StyletronProvider } from "styletron-react";
import { Client as Styletron } from "styletron-engine-atomic";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./Home";
import Login from "./Login";
import SignUp from "./SignUp";
import AddProducts from "./components/AddProducts";
import MerchantLogin from "./components/MerchantLogin";
import UserLoggedIn from "./components/UserLoggedIn";
import ShoppingCartDisplay from "./components/ShoppingCartDisplay";
import { LightTheme, BaseProvider } from "baseui";
import { Client, Server } from "styletron-engine-atomic";
// Create Styletron engine instance
const engine = typeof window === "undefined" ? new Server() : new Client();

// Wrap your entire React application with StyletronProvider
const App = () => (
  <StyletronProvider value={engine}>
    <BaseProvider theme={LightTheme}>
      <Router>
        <Routes>
          <Route path="/" element={<Home></Home>}></Route>
          <Route path="/login" element={<Login></Login>}></Route>
          <Route path="/signUp" element={<SignUp></SignUp>}></Route>
          <Route
            path="/add-products"
            element={<AddProducts></AddProducts>}
          ></Route>
          <Route
            path="/add-products"
            element={<AddProducts></AddProducts>}
          ></Route>
          <Route path="/merchantpage" element={<MerchantLogin />} />
          <Route path="/loginpage" element={<UserLoggedIn />} />
          <Route path="/cart" element={<ShoppingCartDisplay />} />
          <Route path="/" element={<></>}></Route>
        </Routes>
      </Router>
    </BaseProvider>
  </StyletronProvider>
);

export default App;
